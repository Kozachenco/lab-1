import java.util.Scanner;

public class Laba {
  public static void main(String[] args) {
    System.out.println("Выбор действия");
    System.out.println("1. простые числа");
    System.out.println("2. палиндром");
    Scanner sc = new Scanner(System.in);
    int i = sc.nextInt(); // Считываем выбор пользователя
    while (i != 1 && i != 2) { // Пока не будет введена 1 или 2
      i = sc.nextInt();
    }
    if (i == 1) {
      prostCh();
    }
    else {
      palindrom();
    }
  }

  // Считывает n и выводит простые числа от 1 до n
  private static void prostCh() {
    System.out.print("до какого числа? ");
    int n = readInt();
    while (n <= 1) { // Пока не будет введено число больше 1
      System.out.println("Число должно быть больше 1");
      n = readInt();
    }
    System.out.println("1"); // 1 тоже является простым числом
    for (int i=2; i <= n; i++) {
      if (isSimple(i)) {
        System.out.println(i);
      }
    }
  }

  // Считывает фразу и проверяет ее на палиндром
  private static void palindrom() {
    String str = readString(); // Считываем фразу
    boolean p = isPalindrome(str); // и проверяем является ли она палиндромом
    if (p) {
      System.out.println("палиндром");
    }
    else {
      System.out.println("не палидром");
    }
  }

  private static int readInt() {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    return n;
  }

  private static String readString() {
    Scanner sc = new Scanner(System.in);
    String str = sc.nextLine();
    return str;
  }

  private static boolean isSimple(int n) {
    for (int i=2; i < n; i++) { // Проходим по всем числам от 2 до n
      if (n % i == 0) { // если n делится на какое-то число кроме 1 и себя, то оно не простое 
        return false;
      }
    }
    return true; // иначе оно простое
  }

  private static boolean isPalindrome(String str) {
    int c;
    if (str.length() % 2 == 0) { // если длина строки четная
      c = str.length() / 2; // то центром будет символ после середины
    } else {
      c = (str.length() - 1) / 2; // иначе берем символ по центру
    }
    for (int i = 0; i <= c; i++) {
      if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
        return false;
      }
    }
    return true;
  }
}
